package lange.sven.threchner.zustand;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;

import java.util.regex.Pattern;

import static lange.sven.threchner.Einheiten.*;
import static lange.sven.threchner.Utils.*;

/**
 * Klasse für isotherme Zustandsgleichung
 */
public class Isotherm {

    public static Panel panelAnzeigen(WindowBasedTextGUI textGUI) {

        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(7));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Volumen1:").addTo(panel);
        final TextBox volumenEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl1 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen
        panel.addComponent(new EmptySpace(new TerminalSize(10, 0))); // Abstand
        
        new Label("Volumen2:").addTo(panel);
        final TextBox volumenEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl2 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen
        for (int i = 0; i<7; i++) panel.addComponent(new EmptySpace(new TerminalSize(0, 1))); // Abstand

        new Label("Druck1:").addTo(panel);
        final TextBox druckEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Druckes
        ComboBox<String> druckAuswahl1 = comboBox(DRUCK).addTo(panel); // Auswahl der Einheit für den Druck
        panel.addComponent(new EmptySpace(new TerminalSize(10, 0))); // Abstand
        new Label("Druck2:").addTo(panel);
        
        final TextBox druckEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Druckes
        ComboBox<String> druckAuswahl2 = comboBox(DRUCK).addTo(panel); // Auswahl der Einheit für den Druck
        for (int i = 0; i<7; i++) panel.addComponent(new EmptySpace(new TerminalSize(0, 1))); // Abstand

        new Button("Umrechnen", () -> {

            // Abfragen der eingeben Werte
            String vS1 = volumenEingabe1.getText();
            String vS2 = volumenEingabe2.getText();
            String dS1 = druckEingabe1.getText();
            String dS2 = druckEingabe2.getText();

            // Überprüfen der Eingabe
            if (!korrekteEingabe(new String[] {vS1, vS2, dS1, dS2})){
                MessageDialog.showMessageDialog(textGUI, "Fehler", "Ein Feld muss leer gelassen werden!");
                return;
            }

            if (vS1.isBlank()) {

                // Umwandeln der Werte
                double volumen2 = Double.parseDouble(vS2);
                double druck1 = Double.parseDouble(dS1);
                double druck2 = Double.parseDouble(dS2);

                if (isNull(druck1)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Druck1 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double vSI2 = umrechnen(volumen2, vf2);
                double dSI1 = umrechnen(druck1, df1);
                double dSI2 = umrechnen(druck2, df2);

                double ergebnis = umrechnen(vSI2 * dSI2 / dSI1, vf1);
                volumenEingabe1.setText(Double.toString(ergebnis));
            }
            else if (vS2.isBlank()) {

                // Umwandeln der Werte
                double volumen1 = Double.parseDouble(vS1);
                double druck1 = Double.parseDouble(dS1);
                double druck2 = Double.parseDouble(dS2);

                if (isNull(druck2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Druck2 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double vSI1 = umrechnen(volumen1, vf1);
                double dSI1 = umrechnen(druck1, df1);
                double dSI2 = umrechnen(druck2, df2);

                double ergebnis = umrechnen(vSI1 * dSI1 / dSI2, vf2);
                volumenEingabe2.setText(Double.toString(ergebnis));
            }
            else if (dS1.isBlank()) {

                // Umwandeln der Werte
                double volumen1 = Double.parseDouble(vS1);
                double volumen2 = Double.parseDouble(vS2);
                double druck2 = Double.parseDouble(dS2);

                if (isNull(volumen2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Volumen2 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double vSI1 = umrechnen(volumen1, vf1);
                double vSI2 = umrechnen(volumen2, vf2);
                double dSI2 = umrechnen(druck2, df2);

                double ergebnis = umrechnen(vSI1 * dSI2 / vSI2, df1);
                druckEingabe1.setText(Double.toString(ergebnis));
            }
            else if (dS2.isBlank()) {

                // Umwandeln der Werte
                double volumen1 = Double.parseDouble(vS1);
                double volumen2 = Double.parseDouble(vS2);
                double druck1 = Double.parseDouble(dS1);

                if (isNull(volumen1)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Volumen1 muss ungleich 0 sein");
                    return;
                }


                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double vSI1 = umrechnen(volumen1, vf1);
                double vSI2 = umrechnen(volumen2, vf2);
                double dSI1 = umrechnen(druck1, df1);

                double ergebnis = umrechnen(vSI2 * dSI1 / vSI1, df2);
                druckEingabe2.setText(Double.toString(ergebnis));
            }

        }).addTo(panel);
        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }
}
