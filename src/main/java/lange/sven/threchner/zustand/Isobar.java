package lange.sven.threchner.zustand;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;

import java.util.regex.Pattern;

import static lange.sven.threchner.Einheiten.*;
import static lange.sven.threchner.Utils.*;

/**
 * Klasse für isobare Zustandsgleichung
 */
public class Isobar {

    public static Panel panelAnzeigen(WindowBasedTextGUI textGUI) {

        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(7));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Volumen1:").addTo(panel);
        final TextBox volumenEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl1 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen
        panel.addComponent(new EmptySpace(new TerminalSize(10, 0))); // Abstand

        new Label("Volumen2:").addTo(panel);
        final TextBox volumenEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl2 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen
        for (int i = 0; i<7; i++) panel.addComponent(new EmptySpace(new TerminalSize(0, 1))); // Abstand

        new Label("Temperatur1:").addTo(panel);
        final TextBox temperaturEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Temperatures
        ComboBox<String> temperaturAuswahl1 = comboBox(TEMPERATUR).addTo(panel); // Auswahl der Einheit für die Temperatur
        panel.addComponent(new EmptySpace(new TerminalSize(10, 0))); // Abstand

        new Label("Temperatur2:").addTo(panel);
        final TextBox temperaturEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Temperatures
        ComboBox<String> temperaturAuswahl2 = comboBox(TEMPERATUR).addTo(panel); // Auswahl der Einheit für die Temperatur
        for (int i = 0; i<7; i++) panel.addComponent(new EmptySpace(new TerminalSize(0, 1))); // Abstand

        new Button("Umrechnen", () -> {

            // Abfragen der eingeben Werte
            String tS1 = temperaturEingabe1.getText();
            String tS2 = temperaturEingabe2.getText();
            String vS1 = volumenEingabe1.getText();
            String vS2 = volumenEingabe2.getText();

            if (!korrekteEingabe(new String[] {tS1, tS2, vS1, vS2})){
                MessageDialog.showMessageDialog(textGUI, "Fehler", "Ein Feld muss leer gelassen werden!");
                return;
            }

            if (tS1.isBlank()) {

                // Umwandeln der Werte
                double temperatur2 = Double.parseDouble(tS2);
                double volumen1 = Double.parseDouble(vS1);
                double volumen2 = Double.parseDouble(vS2);

                if (isNull(volumen1)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Volumen2 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI2 = temperatur(temperatur2, temperaturAuswahl2.getSelectedItem());
                double vSI1 = umrechnen(volumen1, vf1);
                double vSI2 = umrechnen(volumen2, vf2);

                // V1 * T2 / V2
                double ergebnis = temperatur(vSI1 * tSI2 / vSI2, temperaturAuswahl1.getSelectedItem());
                temperaturEingabe1.setText(Double.toString(ergebnis));
            }
            else if (tS2.isBlank()) {

                // Umwandeln der Werte
                double temperatur1 = Double.parseDouble(tS1);
                double volumen1 = Double.parseDouble(vS1);
                double volumen2 = Double.parseDouble(vS2);

                if (isNull(volumen1)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Volumen1 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI1 = temperatur(temperatur1, temperaturAuswahl1.getSelectedItem());
                double vSI1 = umrechnen(volumen1, vf1);
                double vSI2 = umrechnen(volumen2, vf2);

                // V2 * T1 / V1
                double ergebnis = temperatur(vSI2 * tSI1 / vSI1, temperaturAuswahl2.getSelectedItem());
                temperaturEingabe2.setText(Double.toString(ergebnis));
            }
            else if (vS1.isBlank()) {

                // Umwandeln der Werte
                double temperatur1 = Double.parseDouble(tS1);
                double temperatur2 = Double.parseDouble(tS2);
                double volumen2 = Double.parseDouble(vS2);

                if (isNull(temperatur1) || isNull(temperatur2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Temperatur1/2 müssen ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI1 = temperatur(temperatur1, temperaturAuswahl1.getSelectedItem());
                double tSI2 = temperatur(temperatur2, temperaturAuswahl2.getSelectedItem());
                double vSI2 = umrechnen(volumen2, vf2);

                // V2 / T2 * T1
                double ergebnis = umrechnen(vSI2 / tSI1 * tSI2, vf1);
                volumenEingabe1.setText(Double.toString(ergebnis));
            }
            else if(vS2.isBlank()) {

                // Umwandeln der Werte
                double temperatur1 = Double.parseDouble(tS1);
                double temperatur2 = Double.parseDouble(tS2);
                double volumen1 = Double.parseDouble(vS1);

                if (isNull(temperatur1) || isNull(temperatur2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Temperatur1/2 müssen ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double vf1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
                double vf2 = getVolumen().get(volumenAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI1 = temperatur(temperatur1, temperaturAuswahl1.getSelectedItem());
                double tSI2 = temperatur(temperatur2, temperaturAuswahl2.getSelectedItem());
                double vSI1 = umrechnen(volumen1, vf1);

                // V1 / T1 * T2
                double ergebnis = umrechnen(vSI1 / tSI2 * tSI1, vf2);
                volumenEingabe2.setText(Double.toString(ergebnis));
            }

        }).addTo(panel);
        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }
}
