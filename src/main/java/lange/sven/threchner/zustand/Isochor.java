package lange.sven.threchner.zustand;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;

import java.util.regex.Pattern;

import static lange.sven.threchner.Einheiten.*;
import static lange.sven.threchner.Utils.*;

/**
 * Klasse für isochore Zustandsgleichung
 */
public class Isochor {

    public static Panel panelAnzeigen(WindowBasedTextGUI textGUI) {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(7));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Druck1:").addTo(panel);
        final TextBox druckEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Drucks
        ComboBox<String> druckAuswahl1 = comboBox(DRUCK).addTo(panel); // Auswahl der Einheit für den Druck
        panel.addComponent(new EmptySpace(new TerminalSize(10, 0))); // Abstand

        new Label("Druck2:").addTo(panel);
        final TextBox druckEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Drucks
        ComboBox<String> druckAuswahl2 = comboBox(DRUCK).addTo(panel); // Auswahl der Einheit für das Volumen
        for (int i = 0; i<7; i++) panel.addComponent(new EmptySpace(new TerminalSize(0, 1))); // Abstand

        new Label("Temperatur1:").addTo(panel);
        final TextBox temperaturEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Temperatures
        ComboBox<String> temperaturAuswahl1 = comboBox(TEMPERATUR).addTo(panel); // Auswahl der Einheit für die Temperatur
        panel.addComponent(new EmptySpace(new TerminalSize(10, 0))); // Abstand

        new Label("Temperatur2:").addTo(panel);
        final TextBox temperaturEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)|(Infinity)")).addTo(panel); // Eingabe des Temperatures
        ComboBox<String> temperaturAuswahl2 = comboBox(TEMPERATUR).addTo(panel); // Auswahl der Einheit für die Temperatur
        for (int i = 0; i<7; i++) panel.addComponent(new EmptySpace(new TerminalSize(0, 1))); // Abstand

        new Button("Umrechnen", () -> {
            // Abfragen der eingeben Werte
            String tS1 = temperaturEingabe1.getText();
            String tS2 = temperaturEingabe2.getText();
            String dS1 = druckEingabe1.getText();
            String dS2 = druckEingabe2.getText();

            if (!korrekteEingabe(new String[] {tS1, tS2, dS1, dS2})){
                MessageDialog.showMessageDialog(textGUI, "Fehler", "Ein Feld muss leer gelassen werden!");
                return;
            }

            if (tS1.isBlank()) {

                // Umwandeln der Werte
                double temperatur2 = Double.parseDouble(tS2);
                double druck1 = Double.parseDouble(dS1);
                double druck2 = Double.parseDouble(dS2);

                if (isNull(druck2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Druck2 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI2 = temperatur(temperatur2, temperaturAuswahl2.getSelectedItem());
                double dSI1 = umrechnen(druck1, df1);
                double dSI2 = umrechnen(druck2, df2);

                // P1 * T2 / P2
                double ergebnis = temperatur(dSI1 * tSI2 / dSI2, temperaturAuswahl1.getSelectedItem());
                temperaturEingabe1.setText(Double.toString(ergebnis));
            }
            else if (tS2.isBlank()) {

                // Umwandeln der Werte
                double temperatur1 = Double.parseDouble(tS1);
                double druck1 = Double.parseDouble(dS1);
                double druck2 = Double.parseDouble(dS2);

                if (isNull(druck1)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Druck1 muss ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI1 = temperatur(temperatur1, temperaturAuswahl1.getSelectedItem());
                double dSI1 = umrechnen(druck1, df1);
                double dSI2 = umrechnen(druck2, df2);

                // P2 * T1 / P1
                double ergebnis = temperatur(dSI2 * tSI1 / dSI1, temperaturAuswahl2.getSelectedItem());
                temperaturEingabe2.setText(Double.toString(ergebnis));
            }
            else if (dS1.isBlank()) {

                // Umwandeln der Werte
                double temperatur1 = Double.parseDouble(tS1);
                double temperatur2 = Double.parseDouble(tS2);
                double druck2 = Double.parseDouble(dS2);

                if (isNull(temperatur1) || isNull(temperatur2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Temperatur1/2 müssen ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI1 = temperatur(temperatur1, temperaturAuswahl1.getSelectedItem());
                double tSI2 = temperatur(temperatur2, temperaturAuswahl2.getSelectedItem());
                double dSI2 = umrechnen(druck2, df2);

                // P2 / T2 * T1
                double ergebnis = umrechnen(dSI2 / tSI1 * tSI2, df1);
                druckEingabe1.setText(Double.toString(ergebnis));
            }
            else if (dS2.isBlank()) {

                // Umwandeln der Werte
                double temperatur1 = Double.parseDouble(tS1);
                double temperatur2 = Double.parseDouble(tS2);
                double druck1 = Double.parseDouble(dS1);

                if (isNull(temperatur1) || isNull(temperatur2)) {
                    MessageDialog.showMessageDialog(textGUI, "Fehler", "Temperatur1/2 müssen ungleich 0 sein");
                    return;
                }

                // Abfragen der Faktoren
                double df1 = getDruck().get(druckAuswahl1.getSelectedItem());
                double df2 = getDruck().get(druckAuswahl2.getSelectedItem());

                // Umrechnen in SI-Einheiten
                double tSI1 = temperatur(temperatur1, temperaturAuswahl1.getSelectedItem());
                double tSI2 = temperatur(temperatur2, temperaturAuswahl2.getSelectedItem());
                double dSI1 = umrechnen(druck1, df1);

                // P1 / T1 * T2
                double ergebnis = umrechnen(dSI1 / tSI2 * tSI1, df2);
                druckEingabe2.setText(Double.toString(ergebnis));
            }

        }).addTo(panel);
        // Reset Button
        reset(panel).addTo(panel);

        return panel;

    }
}
