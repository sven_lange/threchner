package lange.sven.threchner;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;

import java.util.List;
import java.util.regex.Pattern;

import static lange.sven.threchner.Einheiten.*;
import static lange.sven.threchner.Utils.*;

public class ThermRechner extends Fenster{

    private final BasicWindow window = new BasicWindow();

    public ThermRechner(MultiWindowTextGUI gui) {
        super(gui);
    }

    @Override
    public BasicWindow anzeigen() {
        window.setHints(List.of(Window.Hint.CENTERED));

        // Erstellen vom Haupt panel
        Panel mainPanel = new Panel();
        mainPanel.setLayoutManager(new BorderLayout());
        window.setComponent(mainPanel);

        // Erstellen von Auswahl panel zur Wahl des Prozesses
        Panel radioPanel = new Panel();
        mainPanel.addComponent(radioPanel.withBorder(Borders.singleLine("Prozess"))).setLayoutData(BorderLayout.Location.LEFT);

        // Erstellen von ProzessPanel
        final Panel[] prozess = {innereEnergie().addTo(mainPanel)};

        // Zurück & Beenden Button
        Panel menu = menu(gui, window).addTo(mainPanel).setLayoutData(BorderLayout.Location.BOTTOM);

        // RadioBox zur Auswahl der Energien
        RadioBoxList<String> radioBoxList = new RadioBoxList<>(new TerminalSize(200,3));
        radioBoxList.addTo(radioPanel);
        // Hinzufügen der einzelnen Elemente
        radioBoxList.addItem("Innere Energie");
        radioBoxList.addItem("Isobare Arbeit");
        radioBoxList.addItem("Isotherme Arbeit");
        radioBoxList.setCheckedItem("Innere Energie");

        // Überprüfen, ob die Auswahl geändert wurde
        radioBoxList.addListener((i, i1) -> {

            mainPanel.removeComponent(prozess[0]); // Entfernen des alten Prozess-panels
            mainPanel.removeComponent(menu); // Entfernen des alten Zurück & Beenden Buttons

            switch (i) {
                case 0 -> prozess[0] = innereEnergie();
                case 1 -> prozess[0] = isobar();
                case 2 -> prozess[0] = isotherm();
            }

            mainPanel.addComponent(prozess[0]); // Füge das neue Prozess-panels hinzu
            mainPanel.addComponent(menu);

        });

        return window;
    }

    /**
     * Panel für die Berechnung der Änderung der inneren Energie
     */
    private Panel innereEnergie() {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(13));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Masse:").addTo(panel);
        final TextBox masseEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Masse
        ComboBox<String> masseAuswahl = comboBox(MASSE).addTo(panel); // Auswahl der Einheit für die Masse

        new Label("c:").addTo(panel);
        final TextBox warmeEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der spezifischen Wärmekapazität
        new Label("J/(kg*K)").addTo(panel); // Hinweis zur spezifischen Wärmekapazität

        new Label("ΔT:").addTo(panel);
        final TextBox temperaturEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Temperaturunterschiedes
        new Label("C|K").addTo(panel); // Hinweis zu Temperatur

        new Label("Änderung innere Energie:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> outputAuswahl = comboBox(ENERGIE).addTo(panel);

        new Button("Berechnen", () -> {

            // Abfragen der eingegebenen Werte
            String mS = masseEingabe.getText();
            String wS = warmeEingabe.getText();
            String tS = temperaturEingabe.getText();

            // Überprüfen ob Eingabe
            if (mS.isEmpty() || wS.isEmpty() || tS.isEmpty()) return;

            // Umwandeln der Werte
            double masse = Double.parseDouble(mS);
            double warme = Double.parseDouble(wS);
            double temperatur = Double.parseDouble(tS);

            //Abfragen der Faktoren
            double fMasse = getMasse().get(masseAuswahl.getSelectedItem());
            double fEnergie = getEnergie().get(outputAuswahl.getSelectedItem());

            // Umrechnen in SI-Einheiten
            double mSI = umrechnen(masse, fMasse);

            double ergebnis = umrechnen(mSI * warme * temperatur, 1., fEnergie);

            output.setText(Double.toString(ergebnis));

        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }

    /**
     * Panel für die Berechnung der verrichteten Arbeit
     */
    private Panel isobar() {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(13));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Druck:").addTo(panel);
        final TextBox druckEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Druckes
        ComboBox<String> druckAuswahl = comboBox(DRUCK).addTo(panel); // Auswahl der Einheit für den Druck

        new Label("Volumen1:").addTo(panel);
        final TextBox volumenEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl1 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen

        new Label("Volumen2:").addTo(panel);
        final TextBox volumenEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl2 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen

        new Label("Verrichtete Arbeit:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> outputAuswahl = comboBox(ENERGIE).addTo(panel);

        new Button("Berechnen", () -> {

            // Abfragen der Werte
            String dS = druckEingabe.getText();
            String vS1 = volumenEingabe1.getText();
            String vS2 = volumenEingabe2.getText();

            // Überprüfen ob Eingabe
            if (dS.isEmpty() || vS1.isEmpty() || vS2.isEmpty() ) return;

            // Umwandeln der Werte
            double druck = Double.parseDouble(dS);
            double volumen1 = Double.parseDouble(vS1);
            double volumen2 = Double.parseDouble(vS2);

            // Abfragen der Faktoren
            double fDruck = getDruck().get(druckAuswahl.getSelectedItem());
            double fVolumen1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
            double fVolumen2 = getVolumen().get(volumenAuswahl2.getSelectedItem());
            double fEnergie = getEnergie().get(outputAuswahl.getSelectedItem());

            // Umrechnen in SI-Einheiten
            double dSI = umrechnen(druck, fDruck);
            double vSI1 = umrechnen(volumen1, fVolumen1);
            double vSI2 = umrechnen(volumen2, fVolumen2);

            double ergebnis = umrechnen(-1. * dSI * (vSI2 - vSI1), 1., fEnergie);
            output.setText(Double.toString(ergebnis));

        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }

    private Panel isotherm() {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(19));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Masse:").addTo(panel);
        final TextBox masseEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Masse
        ComboBox<String> masseAuswahl = comboBox(MASSE).addTo(panel); // Auswahl der Einheit für die Masse

        new Label("Rᵢ:").addTo(panel);
        final TextBox gasEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der spezifischen Gaskonstante
        new Label("J/(kg*K)").addTo(panel); // Hinweis zur spezifischen Gaskonstanten

        new Label("Temperatur:").addTo(panel);
        final TextBox temperaturEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der absoluten Temperatur
        ComboBox<String> temperaturAuswahl = comboBox(TEMPERATUR).addTo(panel);

        new Label("Volumen1:").addTo(panel);
        final TextBox volumenEingabe1 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl1 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen

        new Label("Volumen2:").addTo(panel);
        final TextBox volumenEingabe2 = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Volumens
        ComboBox<String> volumenAuswahl2 = comboBox(VOLUMEN).addTo(panel); // Auswahl der Einheit für das Volumen

        new Label("Verrichtete Arbeit:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> outputAuswahl = comboBox(ENERGIE).addTo(panel);

        new Button("Berechnen", () -> {

            // Abfragen der eingeben Werte
            String mS = masseEingabe.getText();
            String gS = gasEingabe.getText();
            String tS = temperaturEingabe.getText();
            String vS1 = volumenEingabe1.getText();
            String vS2 = volumenEingabe2.getText();

            // Überprüfen ob Eingabe
            if (mS.isEmpty() || gS.isEmpty() || tS.isEmpty() || vS1.isEmpty() || vS2.isEmpty() ) return;

            // Umwandeln der Werte
            double masse = Double.parseDouble(mS);
            double gas = Double.parseDouble(gS);
            double temperatur = Double.parseDouble(tS);
            double volumen1 = Double.parseDouble(vS1);
            double volumen2 = Double.parseDouble(vS2);

            if (isNull(volumen1)) {
                MessageDialog.showMessageDialog(new MultiWindowTextGUI(gui.getScreen()), "Fehler", "Volumen1 muss ungleich 0 sein");
                return;
            }

            //Abfragen der Faktoren
            double fMasse = getMasse().get(masseAuswahl.getSelectedItem());
            double fVolumen1 = getVolumen().get(volumenAuswahl1.getSelectedItem());
            double fVolumen2 = getVolumen().get(volumenAuswahl2.getSelectedItem());
            double fEnergie = getEnergie().get(outputAuswahl.getSelectedItem());

            // Umrechnen in SI-Einheiten
            double mSI = umrechnen(masse, fMasse);
            double vSI1 = umrechnen(volumen1, fVolumen1);
            double vSI2 = umrechnen(volumen2, fVolumen2);

            double ergebnis = umrechnen(-1. * mSI * gas * temperatur(temperatur, temperaturAuswahl.getSelectedItem()) * Math.log(vSI2 / vSI1), 1., fEnergie);
            output.setText(Double.toString(ergebnis));

        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }
}
