package lange.sven.threchner;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;

import java.util.List;
import java.util.regex.Pattern;

import static lange.sven.threchner.Einheiten.*;
import static lange.sven.threchner.Utils.comboBox;
import static lange.sven.threchner.Utils.menu;

/**
 * Fenster für manuelle Einheitenumrechnung
 * @author Sven Lange
 */
public class Umrechnen extends Fenster {

    private final BasicWindow window = new BasicWindow();

    public Umrechnen(MultiWindowTextGUI gui) {
        super(gui);
    }

    @Override
    public BasicWindow anzeigen() {

        window.setHints(List.of(Window.Hint.CENTERED));

        // Erstellen vom Haupt panel
        Panel mainPanel = new Panel();
        mainPanel.setLayoutManager(new LinearLayout(Direction.HORIZONTAL));
        window.setComponent(mainPanel);

        // Erstellen von Auswahl panel zur Wahl der Einheit
        Panel radioPanel = new Panel();
        mainPanel.addComponent(radioPanel.withBorder(Borders.singleLine("Einheiten")));

        // Erstellen von Umrechnen panel zum Umrechnen der Einheiten
        final Panel[] umrechnen = {umrechnenPanel(VOLUMEN).addTo(mainPanel)};

        // RadioBox zur Auswahl der Einheit
        RadioBoxList<String> radioBoxList = new RadioBoxList<>(new TerminalSize(14,6));
        radioBoxList.addTo(radioPanel);
        // Hinzufügen der einzelnen Elemente
        radioBoxList.addItem(VOLUMEN);
        radioBoxList.addItem(DRUCK);
        radioBoxList.addItem(TEMPERATUR);
        radioBoxList.addItem(ENERGIE);
        radioBoxList.addItem(MASSE);
        radioBoxList.addItem(STRECKE);
        radioBoxList.setCheckedItem(VOLUMEN); // Volumen als Default setzten

        // Überprüfen, ob die Auswahl geändert wurde
        radioBoxList.addListener((i, i1) -> {

            mainPanel.removeComponent(umrechnen[0]); // Entferne das alte Umrechnen panel

            switch (i) {
                case 0 -> umrechnen[0] = umrechnenPanel(VOLUMEN);
                case 1 -> umrechnen[0] = umrechnenPanel(DRUCK);
                case 2 -> umrechnen[0] = umrechnenPanel(TEMPERATUR);
                case 3 -> umrechnen[0] = umrechnenPanel(ENERGIE);
                case 4 -> umrechnen[0] = umrechnenPanel(MASSE);
                case 5 -> umrechnen[0] = umrechnenPanel(STRECKE);
            }

            mainPanel.addComponent(umrechnen[0]); // Füge das neue Umrechnen panel hinzu
        });

        return window;
    }

    /**
     * Panel zur Umrechnung
     * @param auswahl Die ausgewählte Einheit
     * @return Umrechnen Panel
     */
    private Panel umrechnenPanel(String auswahl) {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(5));

        // Eingabe Feld, erster Charakter muss eine Zahl sein, ein Punkt ist erlaubt
        final TextBox input = new TextBox().setValidationPattern(Pattern.compile("(\\d+\\.?\\d*)")).addTo(panel);

        ComboBox<String> comboBox1 = comboBox(auswahl).addTo(panel); // Auswahl der gegebenen Einheit
        final Label output = new Label(""); // Erstellen des Ausgabenfeldes, hier noch leer
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        output.addTo(panel);
        ComboBox<String> comboBox2 = comboBox(auswahl).addTo(panel); // Auswahl der auszugebenden Einheit

        if (auswahl.equalsIgnoreCase(TEMPERATUR)) {

            new Button("Umrechnen", () -> {

                // Überprüfen ob Eingabe
                String iS = input.getText();
                if (iS.isEmpty()) return;
                double wert = Double.parseDouble(iS);

                // Temperatur umrechnen
                // Siehe Utils Klasse
                double ergebnis = temperatur(wert,comboBox1.getSelectedItem(), comboBox2.getSelectedItem());

                output.setText(Double.toString(ergebnis));
            }).addTo(panel);
        }
        else {
            new Button("Umrechnen", () -> {

                // Überprüfen ob Eingabe
                String iS = input.getText();
                if (iS.isEmpty()) return;
                double wert = Double.parseDouble(iS);

                double faktor1;
                double faktor2;

                // Ersten und zweiten Faktor abfragen
                switch (auswahl) {
                    case VOLUMEN -> {
                        faktor1 = getVolumen().get(comboBox1.getSelectedItem());
                        faktor2 = getVolumen().get(comboBox2.getSelectedItem());
                    }
                    case DRUCK -> {
                        faktor1 = getDruck().get(comboBox1.getSelectedItem());
                        faktor2 = getDruck().get(comboBox2.getSelectedItem());
                    }
                    case ENERGIE -> {
                        faktor1 = getEnergie().get(comboBox1.getSelectedItem());
                        faktor2 = getEnergie().get(comboBox2.getSelectedItem());
                    }
                    case MASSE -> {
                        faktor1 = getMasse().get(comboBox1.getSelectedItem());
                        faktor2 = getMasse().get(comboBox2.getSelectedItem());
                    }
                    case STRECKE -> {
                        faktor1 = getStrecke().get(comboBox1.getSelectedItem());
                        faktor2 = getStrecke().get(comboBox2.getSelectedItem());
                    }
                    default -> {
                        return;
                    }
                }


                // Wert * Faktor1 * Faktor2
                double ergebnis = umrechnen(wert,faktor1, faktor2);

                output.setText(Double.toString(ergebnis));
            }).addTo(panel);
        }

        menu(gui, window).addTo(panel);

        //Hinweis für die Energien
        if(auswahl.equalsIgnoreCase(ENERGIE)) new Label("1Nm = 1Ws = 1J").addTo(panel);

        return panel;
    }
}
