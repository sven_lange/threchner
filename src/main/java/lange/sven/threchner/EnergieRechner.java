package lange.sven.threchner;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;

import java.util.List;
import java.util.regex.Pattern;

import static lange.sven.threchner.Einheiten.*;
import static lange.sven.threchner.Utils.*;

/**
 * Fenster zur Berechnung von Energien
 * @author Sven Lange
 */
public class EnergieRechner extends Fenster{

    private static final String CHEMISCH = "Chemisch";
    private static final String WARME = "Warme";
    private static final String POTENZIELL = "Potenziell";
    private static final String KINETISCH = "Kinetisch";

    private final BasicWindow window = new BasicWindow();

    public EnergieRechner(MultiWindowTextGUI gui) {
        super(gui);
    }

    @Override
    public BasicWindow anzeigen() {

        window.setHints(List.of(Window.Hint.CENTERED));

        // Erstellen vom Haupt panel
        Panel mainPanel = new Panel();
        mainPanel.setLayoutManager(new BorderLayout());
        window.setComponent(mainPanel);

        // Erstellen von Auswahl panel zur Wahl der Energie
        Panel radioPanel = new Panel();
        mainPanel.addComponent(radioPanel.withBorder(Borders.singleLine("Energien"))).setLayoutData(BorderLayout.Location.LEFT);

        // Erstellen von Energie panel zum Umrechnen der Energien
        final Panel[] energie = {chemisch().addTo(mainPanel)};

        // Zurück & Beenden Button
        Panel menu = menu(gui, window).addTo(mainPanel).setLayoutData(BorderLayout.Location.BOTTOM);

        // RadioBox zur Auswahl der Energien
        RadioBoxList<String> radioBoxList = new RadioBoxList<>(new TerminalSize(14,4));
        radioBoxList.addTo(radioPanel);
        // Hinzufügen der einzelnen Elemente
        radioBoxList.addItem(CHEMISCH);
        radioBoxList.addItem(WARME);
        radioBoxList.addItem(POTENZIELL);
        radioBoxList.addItem(KINETISCH);
        radioBoxList.setCheckedItem(CHEMISCH); //Chemisch als Default setzten

        // Überprüfen, ob die Auswahl geändert wurde
        radioBoxList.addListener((i, i1) -> {

            mainPanel.removeComponent(energie[0]); // Entfernen des alten Energie-panels
            mainPanel.removeComponent(menu); // Entfernen des alten Zurück & Beenden Buttons

            switch (i) {
                case 0 -> energie[0] = chemisch();
                case 1 -> energie[0] = warme();
                case 2 -> energie[0] = potenziell();
                case 3 -> energie[0] = kinetisch();
            }

            mainPanel.addComponent(energie[0]); // Füge das neue Energie panel hinzu
            mainPanel.addComponent(menu);
        });

        return window;
    }

    /**
     * Panel für kinetische Energie
     */
    private Panel kinetisch() {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(10));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Masse:").addTo(panel);
        final TextBox masseEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Masse
        ComboBox<String> masseAuswahl = comboBox(MASSE).addTo(panel); // Auswahl der Einheit für die Masse

        new Label("Geschwindigkeit:").addTo(panel);
        final TextBox geschwindigkeitEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Geschwindigkeit
        new Label("m/s").addTo(panel); // Hinweis zur Geschwindigkeit

        new Label("Ergebnis:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> outputAuswahl = comboBox(ENERGIE).addTo(panel);

        new Button("Berechnen", () -> {

            // Abfragen der eingeben Werte
            String mS = masseEingabe.getText();
            String gS = geschwindigkeitEingabe.getText();

            // Überprüfen ob Eingabe
            if (mS.isEmpty() || gS.isEmpty()) return;

            // Umwandeln der Werte
            double masse = Double.parseDouble(mS);
            double geschwindigkeit = Double.parseDouble(gS);

            // Abfragen der Faktoren
            double fMasse = getMasse().get(masseAuswahl.getSelectedItem());
            double fEnergie = getEnergie().get(outputAuswahl.getSelectedItem());

            // Umrechnen in SI-Einheiten
            double mSI = umrechnen(masse, fMasse);

            double ergebnis = umrechnen(0.5 * mSI * geschwindigkeit * geschwindigkeit, 1., fEnergie);

            output.setText(Double.toString(ergebnis));

        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }

    /**
     * Panel für potenzielle Energie
     */
    private Panel potenziell() {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(13));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Masse:").addTo(panel);
        final TextBox masseEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Masse
        ComboBox<String> masseAuswahl = comboBox(MASSE).addTo(panel); // Auswahl der Einheit für die Masse

        new Label("Beschleunigung:").addTo(panel);
        final TextBox beschleunigungEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).setText("9.81").addTo(panel); // Eingabe der Beschleunigung, Standard: Erdbeschleunigung
        new Label("m/s²").addTo(panel); // Hinweis zur Beschleunigung

        new Label("Höhe:").addTo(panel);
        final TextBox hoheEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Höhe
        ComboBox<String> hoheAuswahl = comboBox(STRECKE).addTo(panel);

        new Label("Ergebnis:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> outputAuswahl = comboBox(ENERGIE).addTo(panel);

        new Button("Berechnen", () -> {

            // Abfragen der eingeben Werte
            String mS = masseEingabe.getText();
            String bS = beschleunigungEingabe.getText();
            String hS = hoheEingabe.getText();

            // Überprüfen ob Eingabe
            if (mS.isEmpty() || hS.isEmpty() || bS.isEmpty()) return;

            // Umwandeln der Werte
            double masse = Double.parseDouble(mS);
            double beschleunigung = Double.parseDouble(bS);
            double hohe = Double.parseDouble(hS);

            // Abfragen der Faktoren
            double fMasse = getMasse().get(masseAuswahl.getSelectedItem());
            double fHohe = getStrecke().get(hoheAuswahl.getSelectedItem());
            double fEnergie = getEnergie().get(outputAuswahl.getSelectedItem());

            // Umrechnen in SI-Einheiten
            double mSI = umrechnen(masse, fMasse);
            double hSI = umrechnen(hohe, fHohe);

            double ergebnis = umrechnen(mSI * beschleunigung * hSI, 1., fEnergie);

            output.setText(Double.toString(ergebnis));

        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }

    /**
     * Panel für Wärmeenergierechner
     */
    private Panel warme() {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(13));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Masse:").addTo(panel);
        final TextBox masseEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Masse
        ComboBox<String> masseAuswahl = comboBox(MASSE).addTo(panel); // Auswahl der Einheit für die Masse

        new Label("c:").addTo(panel);
        final TextBox warmeEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der spezifischen Wärmekapazität
        new Label("J/(kg*K)").addTo(panel); // Hinweis zur spezifischen Wärmekapazität

        new Label("ΔT:").addTo(panel);
        final TextBox temperaturEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Temperaturunterschiedes
        new Label("C|K").addTo(panel); // Hinweis zu Temperatur
        
        new Label("Ergebnis:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> outputAuswahl = comboBox(ENERGIE).addTo(panel);
        
        new Button("Berechnen", () -> {

            // Abfragen der eingeben Werte
            String mS = masseEingabe.getText();
            String wS = warmeEingabe.getText();
            String tS = temperaturEingabe.getText();

            // Überprüfen ob Eingabe
            if (mS.isEmpty() || wS.isEmpty() || tS.isEmpty()) return;

            // Umwandeln der Werte
            double masse = Double.parseDouble(mS);
            double warme = Double.parseDouble(wS);
            double temperatur = Double.parseDouble(tS);

            //Abfragen der Faktoren
            double fMasse = getMasse().get(masseAuswahl.getSelectedItem());
            double fEnergie = getEnergie().get(outputAuswahl.getSelectedItem());

            double SI = umrechnen(masse, fMasse); // Umrechnen in SI-Einheit

            double ergebnis = umrechnen(SI * warme * temperatur, 1, fEnergie);

            output.setText(Double.toString(ergebnis));
        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }

    /**
     * Panel fur chemischen Energierechner
     */
    private Panel chemisch() {

        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(8));

        RadioBoxList<String> radioBoxList = new RadioBoxList<>(new TerminalSize(31,2));
        radioBoxList.addTo(panel);
        radioBoxList.addItem("fester/flüssiger Brennstoff");
        radioBoxList.addItem("gasförmiger Brennstoff");
        radioBoxList.setCheckedItemIndex(0);

        final Panel[] energie = {chemischRechnung(0).addTo(panel)};

        radioBoxList.addListener((i, i1) -> {

            panel.removeComponent(energie[0]); // Entferne das alte Chemische panel

            energie[0] = chemischRechnung(i); // Neues Chemisches-Panel

            panel.addComponent(energie[0]);

        });

        return panel;
    }

    private Panel chemischRechnung(int i) {
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(10));

        // Eingabe Feld, erster Charakter muss eine Zahl oder ein Minus sein, ein Punkt und ein E (-) sind erlaubt
        new Label("Heizwert:").addTo(panel);
        final TextBox heizwertEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe des Heizwertes
        // Hinweis nach Auswahl unterschiedlich
        if(i == 0) new Label("J/kg").addTo(panel);
        else new Label("J/m³").addTo(panel);

        // Hinweis nach Auswahl unterschiedlich
        if ( i == 0) new Label("Masse:").addTo(panel);
        else new Label("Volumen:").addTo(panel);
        final TextBox masseEingabe = new TextBox().setValidationPattern(Pattern.compile("(-?\\d+\\.?\\d*+E?+-?+\\d*)")).addTo(panel); // Eingabe der Masse
        ComboBox<String> auswahl;

        // Auswahl der gegebenen Einheit nach vorheriger Auswahl unterschiedlich
        if(i == 0) auswahl = comboBox(MASSE).addTo(panel);
        else auswahl = comboBox(VOLUMEN).addTo(panel);

        new Label("Ergebnis:").addTo(panel);
        panel.addComponent(new EmptySpace(new TerminalSize(0, 0))); // Abstand für Ausgabe
        final Label output = new Label("").addTo(panel); // Erstellen des Ausgabenfeldes, hier noch leer
        ComboBox<String> energieAuswahl = comboBox(ENERGIE).addTo(panel); // Auswahl der auszugebenden Einheit

        new Button("Berechnen", () -> {

            // Abfragen der eingegebenen Werte
            String hS = heizwertEingabe.getText();
            String mS = masseEingabe.getText();

            // Überprüfen ob Eingabe
            if (hS.isEmpty() || mS.isEmpty()) return;

            // Umwandeln von String in Double
            double heizwert = Double.parseDouble(hS);
            double masse = Double.parseDouble(mS);

            // Abfragen des Faktors, nach vorheriger Auswahl unterschiedlich
            double faktor1;
            if (i == 0) faktor1 = getMasse().get(auswahl.getSelectedItem());
            else faktor1 = getVolumen().get(auswahl.getSelectedItem());

            double SI = umrechnen(masse, faktor1); // Umrechnen in SI-Einheit
            double fEnergie = getEnergie().get(energieAuswahl.getSelectedItem());

            double ergebnis = umrechnen(heizwert * SI, 1, fEnergie);

            output.setText(Double.toString(ergebnis));
        }).addTo(panel);

        // Reset Button
        reset(panel).addTo(panel);

        return panel;
    }

}
