package lange.sven.threchner;

import com.googlecode.lanterna.gui2.*;

import java.util.Collection;

import static lange.sven.threchner.Einheiten.*;

/**
 * Klasse für Frontend Funktionen
 * @author Sven Lange
 */
public final class Utils {

    /**
     * Die Einheiten zur Combo box hinzufügen
     * @param auswahl Die ausgewählte Einheit
     * @return Erstellte Combo box mit allen Einheiten der Auswahl
     */
    public static ComboBox<String> comboBox(String auswahl) {
        ComboBox<String> comboBox = new ComboBox<>();
        switch (auswahl){
            case VOLUMEN:
                for(String einheit : getVolumen().keySet()) comboBox.addItem(einheit);
                break;
            case DRUCK:
                for(String einheit : getDruck().keySet()) comboBox.addItem(einheit);
                break;
            case ENERGIE:
                for (String einheit : getEnergie().keySet()) comboBox.addItem(einheit);
                break;
            case MASSE:
                for (String einheit : getMasse().keySet()) comboBox.addItem(einheit);
                break;
            case STRECKE:
                for (String einheit : getStrecke().keySet()) comboBox.addItem(einheit);
                break;
            case TEMPERATUR:
                comboBox.addItem("K");
                comboBox.addItem("C");
                break;
        }
        return comboBox;
    }

    public static Panel menu(MultiWindowTextGUI gui, BasicWindow window) {

        Panel panel = new Panel();
        panel.setLayoutManager(new LinearLayout(Direction.HORIZONTAL));

        new Button("Zurück", () -> {
            window.close();
            BasicWindow home = new Home(gui).anzeigen();
            gui.addWindowAndWait(home);
        }).addTo(panel);

        new Button("Beenden", window::close).addTo(panel);

        return panel;
    }

    /**
     * Button zum Löschen der Eingaben
     */
    public static Button reset(Panel panel) {
            return new Button("Reset", () -> {
                Collection<Component> components = panel.getChildren();
                panel.removeAllComponents();
                for (Component component : components) {
                    if (component instanceof TextBox textBox) textBox.setText("");
                    panel.addComponent(component);
                }
            });
    }

    /**
     * Überprüft, ob genau ein Feld frei ist
     *
     * @param eingaben zu überprüfende Strings
     * @return ist ein genau ein Feld frei?
     */
    public static boolean korrekteEingabe(String[] eingaben){
        int i = 0;
        for (String eingabe : eingaben) if (eingabe.isBlank()) i++;
        return i == 1;
    }

    /**
     * Überprüft ob Zahl Null ist
     */
    public static boolean isNull(Double number) {
        return number == 0;
    }

}
