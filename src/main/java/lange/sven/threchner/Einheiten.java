package lange.sven.threchner;

import java.util.HashMap;

/**
 * Klasse für das Umrechnen von Einheiten
 * @author Sven Lange
 */
public final class Einheiten {
    public static final String VOLUMEN = "Volumen";
    public static final String DRUCK = "Druck";
    public static final String TEMPERATUR = "Temperatur";
    public static final String ENERGIE = "Energie";
    public static final String MASSE = "Masse";
    public static final String STRECKE = "Strecke";

    private static final HashMap<String, Double> volumen = setVolumen();
    private static final HashMap<String, Double> druck = setDruck();
    private static final HashMap<String, Double> energie = setEnergie();
    private static final HashMap<String, Double> masse = setMasse();
    private static final HashMap<String, Double> strecke = setStrecke();

    private static HashMap<String, Double> setVolumen() {
        HashMap<String, Double> volumen = new HashMap<>();

        //Kubikmeter
        volumen.put("mm3", 1000000000.);
        volumen.put("cm3", 1000000.);
        volumen.put("dm3", 1000.);
        volumen.put("m3", 1.);

        //Liter
        volumen.put("ml", 1000000.);
        volumen.put("cl", 100000.);
        volumen.put("dl", 10000.);
        volumen.put("l", 1000.);
        volumen.put("hl", 10.);

        return volumen;
    }

    private static HashMap<String, Double> setDruck() {
        HashMap<String, Double> druck = new HashMap<>();

        //Druck
        druck.put("pa", 1.0);
        druck.put("hPa", 0.01);
        druck.put("kPa", 0.001);
        druck.put("MPa", 0.000001);
        druck.put("bar", 0.000001);

        return druck;
    }

    private static HashMap<String, Double> setEnergie() {
        HashMap<String, Double> energie = new HashMap<>();

        //Energie
        energie.put("J", 1.);
        energie.put("kJ", 0.001);
        energie.put("MJ", 0.000001);
        energie.put("GJ", 0.000000001);
        energie.put("Wh", 1./3600.);

        return energie;
    }

    private static HashMap<String, Double> setMasse() {
        HashMap<String, Double> masse = new HashMap<>();

        //Masse
        masse.put("mg", 1000000.);
        masse.put("g", 1000.);
        masse.put("kg", 1.);
        masse.put("t", 0.001);

        return masse;
    }

    private static HashMap<String, Double> setStrecke() {
        HashMap<String, Double> strecke = new HashMap<>();

        //Strecke
        strecke.put("mm", 10000.);
        strecke.put("cm", 100.);
        strecke.put("dm", 10.);
        strecke.put("m", 1.);
        strecke.put("km", 0.001);

        return strecke;
    }

    public static HashMap<String, Double> getVolumen() {
        return volumen;
    }
    public static HashMap<String, Double> getDruck(){
        return druck;
    }
    public static HashMap<String, Double> getEnergie() {
        return energie;
    }
    public static HashMap<String, Double> getMasse() {
        return masse;
    }
    public static HashMap<String, Double> getStrecke(){
        return strecke;
    }

    /**
     * Umrechnung in SI-Einheit
     * Volumen/Druck/Energie
     *
     * @param wert Eingegebener Wert
     * @param faktor Faktor für Umrechnung in SI-Einheit
     * @return Wert in SI-Einheit
     */
    public static double umrechnen(double wert, double faktor) {
        return wert / faktor;
    }

    /**
     * Umrechnung von Einheiten
     * Volumen/Druck/Energie/Strecke/Masse
     *
     * @param wert Eingegebener Wert
     * @param faktor1 Faktor für Umrechnung in SI-Einheit
     * @param faktor2 Faktor für Umrechnung von SI-Einheit in ausgewählte Einheit
     * @return Wert in der ausgewählten Einheit
     */
    public static double umrechnen(double wert, double faktor1, double faktor2){

        double ergebnis = wert / faktor1;

        ergebnis = ergebnis * faktor2;

        return ergebnis;
    }

    /**
     * Umrechnung in SI-Einheit
     * Temperatur
     *
     * @param wert Eingegebener Wert
     * @param temperatur Gegebene Einheit
     * @return Wert in SI-Einheit
     */
    public static double temperatur(double wert, String temperatur) {
        // Ist der Wert in Kelvin oder Celsius
        if (temperatur.equalsIgnoreCase("C")) {
            return wert + 273;
        }
        else return wert;
    }

    /**
     * Umrechnung in andere Einheit
     *
     * @param wert Eingegebener Wert
     * @param temperatur1 Gegebene Einheit
     * @param temperatur2 Gesuchte Einheit
     * @return Wert in der ausgewählten Einheit
     */
    public static double temperatur(double wert, String temperatur1, String temperatur2) {
        //Überprüfen ob temperatur 1 und 2 dieselben sind
        if (temperatur1.equalsIgnoreCase("C") && temperatur2.equalsIgnoreCase("K")) {
            return wert + 273;
        } else if (temperatur1.equalsIgnoreCase("K") && temperatur2.equalsIgnoreCase("C")) {
            return wert - 273;
        } else
            return wert;
    }
}
