package lange.sven.threchner;

import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;

/**
 * Vorlage für alle Fenster
 * @author Sven Lange
 */
public abstract class Fenster {
    MultiWindowTextGUI gui;

    public Fenster(MultiWindowTextGUI gui) {
        this.gui = gui;
    }

    /**
     * Methode für das Erstellen von Fenstern
     * @return Erstelltes Fenster zurückgeben
     */
    public abstract BasicWindow anzeigen();
}
