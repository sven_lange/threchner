package lange.sven.threchner;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import lange.sven.threchner.zustand.Isobar;
import lange.sven.threchner.zustand.Isochor;
import lange.sven.threchner.zustand.Isotherm;

import java.util.List;

import static lange.sven.threchner.Utils.menu;

public class Zustandsgleichungen extends Fenster{

    public Zustandsgleichungen(MultiWindowTextGUI gui) {
        super(gui);
    }

    private final BasicWindow window = new BasicWindow();
    private final WindowBasedTextGUI textGUI = new MultiWindowTextGUI(gui.getScreen());

    @Override
    public BasicWindow anzeigen() {

        window.setHints(List.of(Window.Hint.CENTERED));

        // Erstellen vom Haupt panel
        Panel mainPanel = new Panel();
        mainPanel.setLayoutManager(new BorderLayout());
        window.setComponent(mainPanel);

        new Label("Ein Feld freilassen um Wert zu berechnen").setLayoutData(BorderLayout.Location.TOP).addTo(mainPanel);

        // Erstellen von Auswahl panel zur Wahl der Zustandsgleichung
        Panel radioPanel = new Panel();
        mainPanel.addComponent(radioPanel.withBorder(Borders.singleLine("Gleichung"))).setLayoutData(BorderLayout.Location.LEFT);

        // Erstellen von Zustands-panel
        final Panel[] zustand = {Isobar.panelAnzeigen(textGUI).addTo(mainPanel)}; // !!!!!!!!
        zustand[0].setLayoutData(BorderLayout.Location.RIGHT);

        // Zurück & Beenden Button
        Panel menu = menu(gui, window).addTo(mainPanel).setLayoutData(BorderLayout.Location.BOTTOM);

        // RadioBox zur Auswahl der Zustände
        RadioBoxList<String> radioBoxList = new RadioBoxList<>(new TerminalSize(14,4));
        radioBoxList.addTo(radioPanel);
        // Hinzufügen der einzelnen Elemente
        radioBoxList.addItem("Isobar");
        radioBoxList.addItem("Isochor");
        radioBoxList.addItem("Isotherm");
        radioBoxList.setCheckedItem("Isobar");

        // Überprüfen, ob die Auswahl geändert wurde
        radioBoxList.addListener((i, i1) -> {
            mainPanel.removeComponent(zustand[0]); // Entfernen des alten Zustands-panels
            mainPanel.removeComponent(menu); // Entfernen des alten Zurück & Beenden Buttons

            switch (i) {
                case 0 -> zustand[0] = Isobar.panelAnzeigen(textGUI);
                case 1 -> zustand[0] = Isochor.panelAnzeigen(textGUI);
                case 2 -> zustand[0] = Isotherm.panelAnzeigen(textGUI);
            }

            zustand[0].setLayoutData(BorderLayout.Location.RIGHT);
            mainPanel.addComponent(zustand[0]); // Füge das Zustands-panel hinzu
            mainPanel.addComponent(menu);
        });

        return window;
    }
}
