package lange.sven.threchner;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.IOException;

/**
 * Grafischer thermodynamischer Rechner für xterm basierte CMDs.
 * @author Sven Lange
 */
public class Main {

    public static void main(String[] args) throws IOException {

        // Automatisches Erkennen der Terminal Eigenschaften
        Terminal terminal = new DefaultTerminalFactory().createTerminal();

        // Starten des "Bildschirmes" -> "Ausblenden der cmd"
        Screen screen = new TerminalScreen(terminal);
        screen.startScreen();

        // Starten der Grafischen Oberfläche
        MultiWindowTextGUI gui = new MultiWindowTextGUI(screen, new DefaultWindowManager(), new EmptySpace(TextColor.ANSI.BLUE));

        // Öffnen des Homes Fensters
        try {
            gui.addWindowAndWait(new Home(gui).anzeigen());
        }
        catch (Exception e) { // Bei Fehler ausführen
            gui.getActiveWindow().close();
            gui.addWindowAndWait(new Error(gui).anzeigen(e.toString()));
        }

        // Zurück in die CMD nach Schließen des Fensters
        screen.stopScreen();
    }

}
