package lange.sven.threchner;

import com.googlecode.lanterna.gui2.*;

import java.util.List;

/**
 * Home Seite - das Menü
 * @author Sven Lange
 */
public class Home extends Fenster {

    public Home(MultiWindowTextGUI gui) {
        super(gui);
    }

    @Override
    public BasicWindow anzeigen() {

        // Home Fenster erstellen
        BasicWindow window = new BasicWindow();
        window.setHints(List.of(Window.Hint.CENTERED));

        // Panel erstellen und zum Fenster hinzufügen
        Panel panel = new Panel();
        panel.setLayoutManager(new GridLayout(1));
        window.setComponent(panel);

        new Button("Einheiten umrechnen", () -> {
            window.close();
            BasicWindow umrechnen = new Umrechnen(gui).anzeigen();
            gui.addWindowAndWait(umrechnen);
        }).addTo(panel);

        new Button("Energierechner", () -> {
            window.close();
            BasicWindow energierechner = new EnergieRechner(gui).anzeigen();
            gui.addWindowAndWait(energierechner);
        }).addTo(panel);

        new Button("Thermodynamischer Rechner", () -> {
            window.close();
            BasicWindow therm = new ThermRechner(gui).anzeigen();
            gui.addWindowAndWait(therm);
        }).addTo(panel);

        new Button("Zustandsgleichungen", () -> {
            window.close();
            BasicWindow zustand = new Zustandsgleichungen(gui).anzeigen();
            gui.addWindowAndWait(zustand);
        }).addTo(panel);

        new Button("Beenden", window::close).addTo(panel);

        return window;
    }
}
