package lange.sven.threchner;

import com.googlecode.lanterna.gui2.*;

import java.util.List;

/**
 * Fenster für Fehlermeldungen
 */
public class Error {

    MultiWindowTextGUI gui;

    public Error(MultiWindowTextGUI gui) {
        this.gui = gui;
    }

    public BasicWindow anzeigen(String error) {
        BasicWindow window = new BasicWindow();
        window.setHints(List.of(Window.Hint.NO_DECORATIONS, Window.Hint.FULL_SCREEN));

        Panel panel = new Panel();
        panel.setLayoutManager(new LinearLayout(Direction.VERTICAL));
        new Label("Ono, ein Fehler ist aufgetreten! :(").addTo(panel);
        new Label(error).addTo(panel);  // Zeigt Fehler an
        new Button("Beenden", window::close).addTo(panel);
        window.setComponent(panel.withBorder(Borders.singleLine()));

        return window;
    }

}
